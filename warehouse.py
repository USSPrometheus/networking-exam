import socket
from constants import *

server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server_socket.bind((WAREHOUSE_HOST, WAREHOUSE_PORT))


def get_request_feasibility(request_string):
    result = 1

    items = [int(x) for x in request_string.split(',')]

    if len(items) != 5:
        result = 0
    else:
        for item in items:
            if item > 3:
                result = -1
                break

    return result


while True:
    try:
        data, client_address = server_socket.recvfrom(1024)
        request = data.decode()
        print('Request received: ' + request)
        server_socket.sendto(str(get_request_feasibility(data.decode())).encode(), client_address)
    except socket.error as error:
        print('Error:', error)
    except KeyboardInterrupt:
        break

server_socket.close()
