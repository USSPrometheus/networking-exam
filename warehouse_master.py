import select
import socket
import queue
from constants import *

udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server_socket.setblocking(0)
server_socket.bind((WAREHOUSE_MASTER_HOST, WAREHOUSE_MASTER_PORT))
server_socket.listen()

inputs = [server_socket]
outputs = []
message_queues = {}


def parse_request_string(request_string):
    count_list = [int(x) for x in request_string.split(',')]
    sum_of_counts = count_list.pop()
    return count_list, sum_of_counts


def generate_request_string(request_items):
    return ','.join(str(x) for x in request_items)


while inputs:
    readable, writable, exceptional = select.select(inputs, outputs, inputs)

    for peer_socket in readable:
        if peer_socket is server_socket:
            connection, client_address = peer_socket.accept()
            connection.setblocking(0)
            inputs.append(connection)
            message_queues[connection] = queue.Queue()
        else:
            data = peer_socket.recv(1024)
            if data:
                print('Request received: ' + data.decode())

                items, checksum = parse_request_string(data.decode())
                if sum(items) != checksum:
                    print('The request is corrupt')
                    result = b'0'
                else:
                    udp_socket.sendto(generate_request_string(items).encode(), (WAREHOUSE_HOST, WAREHOUSE_PORT))
                    result = udp_socket.recv(2)

                message_queues[peer_socket].put(result)
                if peer_socket not in outputs:
                    outputs.append(peer_socket)
            else:
                if peer_socket in outputs:
                    outputs.remove(peer_socket)
                inputs.remove(peer_socket)
                peer_socket.close()
                del message_queues[peer_socket]

    for peer_socket in writable:
        try:
            next_msg = message_queues[peer_socket].get_nowait()
        except queue.Empty:
            outputs.remove(peer_socket)
        else:
            peer_socket.send(next_msg)

    for peer_socket in exceptional:
        inputs.remove(peer_socket)
        if peer_socket in outputs:
            outputs.remove(peer_socket)
        peer_socket.close()
        del message_queues[peer_socket]
