from constants import *
import socket

client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket.connect((WAREHOUSE_MASTER_HOST, WAREHOUSE_MASTER_PORT))

corrupt_request = False

while True:
    try:
        request_string = input()

        items = request_string.split(',')
        checksum = sum(int(x) for x in items)

        if corrupt_request:
            checksum += 1

        client_socket.sendall(','.join(items + [str(checksum)]).encode())

        response = int(client_socket.recv(2).decode())
        print('The request ' +
              ('is feasible' if response == 1 else ('is not feasible' if response == -1 else 'generated an error')))

        corrupt_request = not corrupt_request
    except KeyboardInterrupt:
        break

client_socket.close()
